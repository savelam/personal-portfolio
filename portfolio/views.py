from django.shortcuts import render

from .models import Project


# Create your views here.

def HomeView(request):
    project = Project.objects.all()
    context = {
        'home': "This is my home page",
        'projects': project
    }
    return render(request, 'portfolio/home.html', context)
